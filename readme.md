# Description
This is a simple maze app which allows the user to create their own customized maze with starting point, destination point and maze walls. It also automatically finds the path based on the maze the user has created.

# Operation
Once the app is running, you can tap one of the cells to set a starting point which will be indicated with a green colour. Tap a different cell to set a destination cell which will be indicated with a red colour. If you change your mind with any of your selections, you may unselect those points by tapping the cells again.
Once you have set the start and end points, you may start creating the maze path by tapping unselected cells to create maze walls which will be indicated with a silver colour. When your maze is finished, tap the "Solve" button to find a path from the starting point to the end point. You will be able to see the maze being solved in real time by tracking the blue path.

# Bugs

- None so far

# Future Work

- Enable rotation to allow the maze to work in both landscape and portrait modes
- Unselect cells that do not lead to the end point
- Identify the location of the end point in order to direct the path towards it