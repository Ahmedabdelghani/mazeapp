package ahmedabdelghani.mazeapp;

import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Owner on 2/19/2016.
 */

public class ButtonAdapter extends BaseAdapter {
    private int testing;
    private final int START_SET = 0;
    private final int START_DISABLED = 1;
    private final int END_SET = 2;
    private final int END_DISABLED = 3;
    private final int WALL_SET = 4;
    private final int WALL_DISABLED = 5;

    //creating a handler
    private Handler handler;

    private boolean done;
    private static ButtonAdapter instance;
    private Context mContext;
    private Maze maze;
    private int columns;
    private int rows = 14;
    private ArrayList<Button> buttons = new ArrayList<>();

    //Creating an array adapter
    public ButtonAdapter(Context context, int numRows, int numColumns) {
        handler = new Handler();
        instance = this;
        mContext = context;
        columns = numColumns;
        rows = numRows;
        maze = new Maze(rows, columns);
    }

    public static ButtonAdapter getInstance() {return instance;}

    // Total number of cells contained within the adapter
    public int getCount() {
        return columns * rows;
    }

    // Required for structure. Not used
    public Object getItem(int position) {
        return null;
    }

    // Get Id of an item in the adapter
    public long getItemId(int position) {
        return position;
    }

    //Start solving the puzzle in the maze class.
    public void solve() {
        maze.beginSolving();
    }

    //Get a empty_button using its position
    public View getView(int position,
                        View convertView, ViewGroup parent) {
        Button button;

        if (convertView == null) {
            // if it's not recycled, initialize some attributes
            button = new Button(mContext);
            button.setBackgroundResource(R.drawable.empty_button);
            button.setLayoutParams(new GridView.LayoutParams(143, 143));
            buttons.add(button);
        }
        else {
            button = (Button) convertView;
        }
        button.setId(position);

        // OnClick listener for each empty_button
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                int id = view.getId();
                testing = maze.click(id);
                if (testing == START_SET) {
                    view.setBackgroundResource(R.drawable.start_button);
                } else if (testing == END_SET) {
                    view.setBackgroundResource(R.drawable.end_button);
                } else if (testing == WALL_SET) {
                    view.setBackgroundResource(R.drawable.wall_button);
                } else if (testing == START_DISABLED) {
                    view.setBackgroundResource(R.drawable.empty_button);
                } else if (testing == END_DISABLED) {
                    view.setBackgroundResource(R.drawable.empty_button);
                } else if (testing == WALL_DISABLED) {
                    view.setBackgroundResource(R.drawable.empty_button);
                }
            }
        });
        return button;
    }

    //update the grid with the solution
    public void updateUI() {

        for(int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setEnabled(false);
        }

        for (int i = 0; i < columns; i++) {
            for (int j = 0; j < rows; j++) {
                if (maze.getState(j, i)) {
                    int pos = j*columns + i;
                    buttons.get(pos).setBackgroundResource(R.drawable.path_button);
                }
            }
        }

        if(maze.isSolved() && !done) {
            done = true;
            Toast toast = Toast.makeText(mContext, R.string.solved_maze, Toast.LENGTH_LONG);
            toast.show();
        }
        else if (maze.getError() && !done) {
            done = true;
            Toast toast = Toast.makeText(mContext, R.string.not_solved, Toast.LENGTH_LONG);
            toast.show();
        }

        for(int i = 0; i < buttons.size(); i++) {
            buttons.get(i).setEnabled(true);
        }
    }

    // getting the handler
    public Handler getHandler() {
        return handler;
    }
}