package ahmedabdelghani.mazeapp;

/**
 * Created by Owner on 2/20/2016.
 */

//A cell class containing the different attribute defining a cell along with
//the required functions to modify and get these values.
// it would have been possible to use regular Set functions that take variables but for this project
//I would rather have different 2 functions for each attribute to easily distinguish when a value
//is being set to true or false.
public class Cell {
    private boolean start = false;
    private boolean end = false;
    private boolean empty = true;
    private boolean wall = false;
    private boolean checked = false;
    private int row;
    private int column;

    public Cell(int row, int column) {
        this.row = row;
        this.column = column;
    }

    //Getters for the Cell class
    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    public boolean getStart() {
        return start;
    }

    public boolean getEnd() {
        return end;
    }

    public boolean getEmpty() {
        return empty;
    }

    public boolean getWall() {
        return wall;
    }

    //Set Start to true and everything else to false
    public boolean setStart() {
        start = true;
        end = false;
        empty = false;
        wall = false;

        return true;
    }

    //Set start to false
    public boolean disableStart() {
        start = false;
        end = false;
        empty = true;
        wall = false;

        return true;
    }

    //return whether the cell has been checked or not
    public boolean getChecked() {
        return checked;
    }

    //set a cell as checked
    public void setChecked(boolean check) {
        checked = check;
    }

    //Set End to true and everything else to false
    public boolean setEnd() {
        start = false;
        end = true;
        empty = false;
        wall = false;

        return true;
    }

    //set End as false
    public boolean disableEnd() {
        start = false;
        end = false;
        empty = true;
        wall = false;

        return true;
    }

    //Set Wall to true and everything else to false
    public boolean setWall() {
        start = false;
        end = false;
        empty = false;
        wall = true;

        return true;
    }

    //Set wall as false
    public boolean disablWall() {
        start = false;
        end = false;
        empty = true;
        wall = false;

        return true;
    }
}
