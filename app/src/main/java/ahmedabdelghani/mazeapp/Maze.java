package ahmedabdelghani.mazeapp;

/**
 * Created by Owner on 2/20/2016.
 */
public class Maze {
    private boolean start = false;
    private boolean end = false;
    private boolean solved;

    private int rows;
    private int columns;
    private Cell[][] cellArray;
    private final int START_SET = 0;
    private final int START_DISABLED = 1;
    private final int END_SET = 2;
    private final int END_DISABLED = 3;
    private final int WALL_SET = 4;
    private final int WALL_DISABLED = 5;
    private final int NIL = -1;

    private boolean running;
    private static boolean error;
    private Cell startingCell;

    //maze constructor
    public Maze(int row, int col) {
        cellArray = new Cell[row][col];
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                cellArray[i][j] = new Cell(i, j);
            }
        }
        rows = row;
        columns = col;
        running = false;
    }

    // error boolean is true if there is no solution
    public boolean getError() {
        return error;
    }

    //function to start the solution thread
    public void start() {
        if (!running) {
            running = true;
            new Thread(new Solver()).start();
        }
    }

    //function to stop the thread
    public void stop() {
        running = false;
    }

    //function to return a boolean indicating if a cell has been checked
    public boolean getState(int x, int y) {
        if (cellArray[x][y].getChecked()) {
            return true;
        } else return false;
    }

    //function used to change the attribute of each cell depending on
    //the existence of start and end cells
    public int click(int pos) {
        int x = pos / columns;
        int y = pos % columns;
        int result = NIL;
        if (!start && !end) {
            cellArray[x][y].setStart();
            start = true;
            result = START_SET;
        } else if (start && !end) {
            if (cellArray[x][y].getStart()) {
                cellArray[x][y].disableStart();
                start = false;
                result = START_DISABLED;
            } else if (cellArray[x][y].getWall()) {
                cellArray[x][y].disablWall();
                result = WALL_DISABLED;
            } else if (cellArray[x][y].getEmpty()) {
                cellArray[x][y].setEnd();
                end = true;
                result = END_SET;
            }
        } else if (!start && end) {
            if (cellArray[x][y].getEmpty()) {
                cellArray[x][y].setStart();
                start = true;
                result = START_SET;
            } else if (cellArray[x][y].getEnd()) {
                cellArray[x][y].disableEnd();
                end = false;
                result = END_DISABLED;
            } else if (cellArray[x][y].getWall()) {
                cellArray[x][y].disablWall();
                result = WALL_DISABLED;
            }

        } else if (start && end) {
            if (cellArray[x][y].getEnd()) {
                cellArray[x][y].disableEnd();
                end = false;
                result = END_DISABLED;
            } else if (cellArray[x][y].getStart()) {
                cellArray[x][y].disableStart();
                start = false;
                result = START_DISABLED;
            } else if (cellArray[x][y].getEmpty()) {
                cellArray[x][y].setWall();
                result = WALL_SET;
            } else if (cellArray[x][y].getWall()) {
                cellArray[x][y].disablWall();
                result = WALL_DISABLED;
            }
        }
        return result;
    }

    //indicate whether the puzzle is solved or not
    public boolean isSolved() {
        return solved;
    }

    // start solving the puzzle by identifying the starting cell
    public void beginSolving() {
        solved = false;
        startingCell = new Cell(0, 0);

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < columns; j++) {
                if (cellArray[i][j].getStart()) {
                    startingCell = cellArray[i][j];
                }
            }
        }
        start();
    }

    //An instance of the solver class to be run on a different thread
    private class Solver implements Runnable {

        //check whether a cell is valid to be visited or not
        public boolean checkCell(int x, int y) {
            if (x >= 0 && y >= 0 && x < rows && y < columns) {
                if (cellArray[x][y].getStart() || cellArray[x][y].getEmpty() || cellArray[x][y].getEnd()) {
                    if (!solved) {
                        return true;
                    }
                }
            }
            return false;
        }

        //update the UI as the computation is running. this function calls
        // the update UI function found in the grid adapter
        public void updateUI() {
            ButtonAdapter.getInstance().getHandler().post(new Runnable() {
                public void run() {
                    ButtonAdapter.getInstance().updateUI();
                }
            });
        }

        //solve the rest of the puzzle after it has been started by the function beginSolving
        public boolean solve(int x, int y) {
            try {
                Thread.sleep(60);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            solved = false;
            error = false;
            updateUI();
            if (checkCell(x, y)) {
                if (!cellArray[x][y].getChecked()) {
                    cellArray[x][y].setChecked(true);
                    if (cellArray[x][y].getEnd()) {
                        solved = true;
                        error = false;
                    }
                    else {
                        if (!solved) {
                            error = false;
                            solved = solve(x + 1, y);
                        }

                        if (!solved) {
                            error = false;
                            solved = solve(x, y + 1);
                        }

                        if (!solved) {
                            error = false;
                            solved = solve(x - 1, y);
                        }

                        if (!solved) {
                            error = false;
                            solved = solve(x, y - 1);
                        }

                        if (!solved) {
                            error = true;
                            stop();
                        }
                    }
                    if (solved) {
                        stop();
                    }
                }
            }
            return solved;
        }

        // Run method used to run the class
        public void run() {
            while (running) {
                solve(startingCell.getRow(), startingCell.getColumn());
            }
        }
    }
}
