package ahmedabdelghani.mazeapp;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.GridView;

public class MainActivity extends AppCompatActivity {
    private Button solve;

    //Select the number of rows and columns
    //the number of cells will be determined accordingly
    private int columns = 10;
    private int rows = 14;

    // Constructor to create a grid adapter. The integer value is the number of cells
    // we want in the maze
    private ButtonAdapter adapter = new ButtonAdapter(this, rows, columns);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.buttons_activity);

        // Creating a grid view adapter
        final GridView gridview = (GridView) findViewById(R.id.grid_view);

        gridview.setNumColumns(columns);

        gridview.setAdapter(adapter);
        solve = (Button) findViewById(R.id.solve_button);

        solve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                adapter.solve();
            }
        });

    }
}
